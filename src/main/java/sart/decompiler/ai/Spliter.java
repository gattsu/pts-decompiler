package sart.decompiler.ai;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.interpreter.Clazz;

public class Spliter {
	public static Path target = Paths.get("classes");
	
	public static void main(String...args) {
		Environment env = new Environment();
		Loader loader = new Loader(env);
		loader.load("ai.obj");
		env.classes.values().forEach(Spliter::write);
		final StringBuilder str = new StringBuilder();
		loader.loadingOrder.forEach(line -> str.append(line).append("\n"));
		flush(target.resolve("order"), str);
	}
	
	public static void write(Clazz cls) {
		final StringBuilder str = new StringBuilder();
		str.append("class ").append(cls.branch).append(" ").append(cls.name).append(" : ").append(cls.parent == null ? "(null)" : cls.parent.name).append("\n");
		cls.handlers.forEach(str::append);
		str.append("class_end");
		
		final Path dir = target.resolve("" + cls.name.charAt(0));
		try {
			if(Files.notExists(dir))
				Files.createDirectories(target.resolve("" + cls.name.charAt(0)));
		} catch (IOException e) { e.printStackTrace(); }
		flush(dir.resolve(cls.name), str);
	}
	
	public static void flush(Path path, StringBuilder str) {
		try {
			Files.write(path, str.toString().getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
		} catch (IOException e) { e.printStackTrace(); }
	}
}
