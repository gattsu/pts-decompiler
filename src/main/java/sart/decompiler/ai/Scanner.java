package sart.decompiler.ai;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.frontend.util.SignatureScanner;

public class Scanner {
	
	public static void main(String...args) {
		Environment env = new Environment();
		Loader loader = new Loader();
		loader.load("ai.obj");
		SignatureScanner scanner = new SignatureScanner(env);
		loader.classes.values().forEach(cls -> {
			cls.handlers.forEach(scanner::parse);
		});
		env.storeSignature("signature");
	}
	
}
