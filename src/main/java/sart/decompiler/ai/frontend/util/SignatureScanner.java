package sart.decompiler.ai.frontend.util;

import java.util.HashMap;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.frontend.Function;
import sart.decompiler.ai.frontend.tree2.TreeParser;
import sart.decompiler.ai.interpreter.Instruction;
import static sart.decompiler.ai.interpreter.Instruction.*;

public class SignatureScanner extends TreeParser {
	
	public SignatureScanner(Environment env) {
		super(env);
	}

	public final HashMap<Integer, Function> functions = new HashMap<>();
	
	@Override
	public void ex_func_call(int index) {
		Function func = env.getFunction(index, true);
		scan: {
			if(func.complete)
				break scan;	
			func.name = getFunctionName();
			Instruction i0 = next(1), i1 = next(2), i2 = next(3);		
			//Template 1
			if(i0.is(SHIFT_SP) && i1 != null && i1.is(SHIFT_SP)) {
				func.argc = -(i0.operand + i1.operand);
				func.ret = false;
				func.complete = true;
				break scan;
			}
			//Template 2
			if(i0.is(SHIFT_SP) && i0.operand < -1) {
				func.argc = -i0.operand + 1;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//Template 3
			if(i0.not(SHIFT_SP)) {
				func.argc = 1;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//Template 4
			//Solution 1
			if(prev(1).not(FETCH_I, PUSH_REG_SP)) {
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//Solution 2
			if(i1 == null || i1.is(LABEL, JUMP, CALL_SUPER, EXIT_HANDLER)) {
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			//Solution 3
			if(stack.size() == 1) {
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			//Solution 4
			if(i1 != null && i1.is(ADD, ADD_STRING, AND, ASSIGN, ASSIGN4, BIT_AND, BIT_OR, BRANCH_TRUE, BRANCH_FALSE, DIV, EQUAL, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, MOD, MUL, NEGATE, NOT, NOT_EQUAL, OR, SUB, FUNC_CALL)) {
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//Solution 4.1
			if(i1 != null && i1.is(PUSH_CONST, PUSH_CONST_F, PUSH_PARAMETER, PUSH_PROPERTY, PUSH_STRING) && i2 != null && i2.is(ADD, ADD_STRING, AND, ASSIGN, ASSIGN4, BIT_AND, BIT_OR, BRANCH_TRUE, BRANCH_FALSE, DIV, EQUAL, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL, MOD, MUL, NEGATE, NOT, NOT_EQUAL, OR, SUB, FUNC_CALL)) {
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//Solution 5 TODO describe
			if(prev(5) != null && prev(5).is(LABEL)) {
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			func.argc = 1;
			func.ret = false;
			func.complete = false;
		}
		super.ex_func_call(index);
	}
	
	public String getFunctionName() {
		String str = getComment().trim();
		return str.substring(5, str.length() - 1);
	}
}
