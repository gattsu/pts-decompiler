package sart.decompiler.ai.frontend.lexer;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Path;

import sart.decompiler.ai.frontend.lexer.Token.ETag;


public class Lexer implements Closeable {
	public Reader reader;
	
	public int line = 1;
	
	public int pos = 0;
	
	public char peek = ' ';
	
	public boolean eof = false;
	
	public Lexer(Reader reader) {
		this.reader = reader;
	}
	
	public Lexer(String file) throws FileNotFoundException {
		this.reader = new BufferedReader(new FileReader(file));
	}

	public boolean readch() throws IOException {
		int ch = reader.read();
		pos++;
		if(ch == -1)
			 return !(eof = true);
		peek = (char) ch;
		return true;
	}

	private Token prev;
	
	private Token[] next;
	
	public Token prev() {
		return prev;
	}
	
	public Token next() {
		if(next == null) {
			next = new Token[5];
			for(int i = 0 ; i < next.length ; i++)
				next[i] = next0();
		}
		prev = next[0];
		for(int i = 0 ; i < next.length - 1 ; i++)
			next[i] = next[i + 1];
		next[next.length - 1] = next0();
		return prev;
	}
	
	public Token next(int idx) { return next[idx]; }
	
	public Token next0() {
		try {
			for(;!eof;readch()) {
				if(peek == ' ' || peek == '\t' || peek == '\r')
					continue;
				else if(peek == '\n') {
					pos = 1;
					line++;
					continue;
				} else 
					break;
			}		
			if(eof)
				return Token.EOS;
			if(peek == ':') {
				readch();
				return new Token(ETag.COLON, ":");
			}
			if(peek == '(') {
				readch();
				return new Token(ETag.LBRACKET, "(");
			}
			if(peek == ')') {
				readch();
				return new Token(ETag.RBRACKET, ")");
			}
			if(peek == '.') {
				readch();
				return new Token(ETag.DOT, ".");
			}
			if(peek == '{') { readch(); return new Token(ETag.LBRACE, "{"); }
			if(peek == '}') { readch(); return new Token(ETag.RBRACE, "}"); }
			if(peek == ';') { readch(); return new Token(ETag.SEMICOLON, ";"); }
			StringBuilder str = new StringBuilder();
			if(peek == '/') {
				if(!readch())
					throw new RuntimeException("eof reached for comment lexem");
				if(peek != '/')
					throw new RuntimeException("syntax error for coment " + line + ":" + pos);
				while(readch() && peek != '\n' && peek != '\r')
					str.append(peek);
				return new Token(ETag.COMMENT, str.toString());
			}
			if(Character.isDigit(peek) || peek == '-') {
				ETag tag = ETag.INT;
				do {
					if(peek == '.')
						tag = ETag.REAL;
					str.append(peek);
				} while(readch() && (Character.isDigit(peek) || peek == '.' || peek == 'E' || peek == '-'));
				return new Token(tag, str.toString());
			}
			
			if(Character.isLetter(peek) || peek == '_') {
				do {
					str.append(peek);
				} while(readch() && (Character.isLetterOrDigit(peek) || peek == '_'));
				return new Token(ETag.WORD, str.toString());
			}
			
			if(peek == '"') {
				while(readch() && peek != '"')
					str.append(peek);	
				readch();//skip closure \"
				return new Token(ETag.STRING, str.toString());
			}
			throw new RuntimeException("unknwon character " + peek + " line " + line + " pos " + pos);
		} catch(IOException e) { return Token.EOS; }
	}
	
	@Override
	public void close() throws IOException {
		reader.close();
	}
	
	//Получить лексический анализатор для текстового файла
	public static Lexer from(Path path) { return null; }
	
	//Получить лексический анализатор для строки
	public static Lexer from(String str) { return new Lexer(new StringReader(str)); }

}
