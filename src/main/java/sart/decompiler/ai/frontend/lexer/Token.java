package sart.decompiler.ai.frontend.lexer;

import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;

public class Token {
	public final ETag tag;
	
	public final String value;

	public Token(ETag tag, String value) {
		this.tag = tag;
		this.value = value;
	}
	
	public final static Token EOS = new Token(ETag.EOS, null);
	
	public enum ETag {
		WORD, // azbd
		INT, // 0
		REAL,// 0.0
		LBRACKET,//(
		RBRACKET,//)
		DOT,// .
		STRING,// "..."
		COMMENT,// //...
		COLON,//:
		LBRACE,//{
		RBRACE,//}
		SEMICOLON,//;
		EOS//End Of Stream
	}
	
	public final boolean isComment() {
		return tag == ETag.COMMENT;
	}
	
	public final boolean isInteger() {
		return tag == ETag.INT;
	}
	
	public final boolean isString() {
		return tag == ETag.STRING;
	}
	
	public final boolean is(ETag tag) { 
		return this.tag == tag;
	}
	
	public final boolean is(String value) { 
		return this.value.equals(value);
	}
	
	public boolean is(ETag tag, IntConsumer f) { return false; }

	public boolean is(ETag tag, DoubleConsumer f) { return false; }
	
	public boolean is(ETag tag, Consumer<String> f) { return false; }
	
	public boolean is(ETag tag, String value) {
		return true;
	}
	
	public final boolean not(ETag tag) { return !is(tag); }
	
	public final boolean not(String value) { return !is(value); }
	
	@Override
	public String toString() {
		return "<" + tag + " , " + value +">"; 
	}

	/*
	 * return token value as basic type int.
	 */
	public final int asInteger() {
		return Integer.parseInt(value);
	}
	
	public final float asFloat() {
		return Float.parseFloat(value);
	}
	
	public final String asString() {
		return value;
	}
}
