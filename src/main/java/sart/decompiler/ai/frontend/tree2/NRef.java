package sart.decompiler.ai.frontend.tree2;

public class NRef extends Node{
	public final Node node;
	
	public NRef(Node node) {
		super(T_REF);
		this.node = node;
	}
}
