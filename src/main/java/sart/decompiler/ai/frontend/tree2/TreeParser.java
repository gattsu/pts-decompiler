package sart.decompiler.ai.frontend.tree2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.frontend.Function;
import sart.decompiler.ai.interpreter.Handler;
import sart.decompiler.ai.interpreter.Interpreter;
import static sart.decompiler.ai.frontend.tree2.Node.*;

public class TreeParser extends Interpreter {
	protected final Environment env;
	
	protected LinkedList<Node> stack = new LinkedList<>();
	
	private final ArrayList<Integer> labels = new ArrayList<>();
	
	public TreeNode tree;
	
	public TreeParser(Environment env) {
		this.env = env;
	}
	
	public void parse(Handler handler) {
		stack.clear();
		labels.clear();
		execute(handler);
		handler.tree = new TreeNode(stack);
	}
	
	public void push(Node node) {
		labels.forEach(node::mark);
		labels.clear();
		stack.push(node);
	}
	
	public Node pop() {
		return stack.pop();
	}

	@Override
	public void onStart() {}
	
	@Override
	public void onFinish() {
		nullary(T_END);
	}
	
	private Node op1, op2;
	
	//instruction with 0 operand
	private final void nullary(int tag) {
		push(Node.of(tag));
	}
	
	//instruction with 1 operand
	private final void unary(int tag) {
		op1 = pop();
		push(Node.of(tag, op1));
	}

	//instruction with 2 operands
	private final void binary(int tag) {
		op2 = pop();
		op1 = pop();
		push(Node.of(tag, op1, op2));
	}
	
	@Override
	public void ex_add() { binary(T_ADD); }
	
	@Override
	public void ex_add_string(int index) { binary(T_CONCAT); }
	
	@Override
	public void ex_and() { binary(T_AND); }
	
	@Override
	public void ex_assign() { binary(T_ASSIGN); }
	
	@Override
	public void ex_assign4() { binary(T_ASSIGN4); }
	
	@Override
	public void ex_bit_and() { binary(T_BIT_AND); }
	
	@Override
	public void ex_bit_or() { binary(T_BIT_OR); }
	
	@Override
	public void ex_branch_false(int label) {
		push(Node.of(T_JFALSE, new NInteger(label)));
	}
	
	@Override
	public void ex_branch_true(int label) {
		push(Node.of(T_JTRUE, new NInteger(label)));
	}
	
	@Override
	public void ex_call_super() { nullary(T_SUPER); }
	
	@Override
	public void ex_div() { binary(T_DIV);}
	
	@Override
	public void ex_equal() { binary(T_EQUAL); }
	
	@Override
	public void ex_exit_handler() { nullary(T_RETURN); }
	
	@Override
	public void ex_fetch_d() { unary(T_FETCHD); }
	
	@Override
	public void ex_fetch_f() { unary(T_FETCHF); }
	
	@Override
	public void ex_fetch_i() { unary(T_FETCH); }
	
	@Override
	public void ex_fetch_i4() { unary(T_FETCH4); }
	
	@Override
	public void ex_func_call(int index) {
		Function func = env.getFunction(index);
		if(func == null)
			throw new RuntimeException(trace() + " not found function " + index + " " + getComment().trim());
		ArrayList<Node> args = new ArrayList<>();
		for(int i = 0 ; i < func.argc ; i++)
			args.add(pop());
		Collections.reverse(args);
		push(Node.of(func, args.toArray(new Node[func.argc])));
	}
	
	@Override
	public void ex_greater() { binary(T_GREATER); }
	
	@Override
	public void ex_greater_equal() { binary(T_GREATER_EQUAL); }
	
	@Override
	public void ex_jump(int label) { 
		push(Node.of(T_JUMP, new NInteger(label))); 
	}
	
	@Override
	public void ex_label(int label) {
		labels.add(label);
	}
	
	@Override
	public void ex_less() { binary(T_LESS); }
	
	@Override
	public void ex_less_equal() { binary(T_LESS_EQUAL); }
	
	@Override
	public void ex_mod() { binary(T_MOD); }
	
	@Override
	public void ex_mul() { binary(T_MUL); }
	
	@Override
	public void ex_negate() { unary(T_NEG); }
	
	@Override
	public void ex_not() { unary(T_NOT); }
	
	@Override
	public void ex_not_equal() { binary(T_NOT_EQUAL); }
	
	@Override
	public void ex_or() { binary(T_OR); }
	
	@Override
	public void ex_push_const(int value) { push(Node.integer(value)); }
	
	@Override
	public void ex_push_const_f(float value) { push(Node.of(value)); }
	
	@Override
	public void ex_push_event() { nullary(T_EVENT); }
	
	@Override
	public void ex_push_parameter(int index) {
		push(Node.of(T_PARAM, new NString(getString(index))));
	}
	
	@Override
	public void ex_push_property(int index) {
		push(Node.of(T_PROP, new NString(getString(index))));
	}
	
	@Override
	public void ex_push_reg_sp() {
		push(Node.ref(pop()));
	}
	
	@Override
	public void ex_push_string(int index) {
		push(Node.of(getMapString(index)));
	}
	
	@Override
	public void ex_sub() { binary(T_SUB); }
	
}
