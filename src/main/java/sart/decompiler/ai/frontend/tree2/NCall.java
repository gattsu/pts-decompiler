package sart.decompiler.ai.frontend.tree2;

import sart.decompiler.ai.frontend.Function;

public class NCall extends Node {
	public final Function func;
	
	public NCall(Function func, Node...args) {
		super(T_CALL, args);
		this.func = func;
	}

}
