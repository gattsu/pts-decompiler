package sart.decompiler.ai.frontend.tree2;

import java.util.Collection;

public class TreeNode {
	public final Node[] roots;
	
	public TreeNode(Collection<Node> nodes) {
		roots = nodes.toArray(new Node[nodes.size()]);
	}
	
	public String toString() {
		return null;
	}
}