package sart.decompiler.ai.frontend.tree2;

public class NString extends Node {
	public final String value;
	
	public NString(String value) {
		super(T_STRING);
		this.value = value;
	}
}
