package sart.decompiler.ai.frontend.tree2;

import java.util.ArrayList;

import sart.decompiler.ai.frontend.Function;

public class Node {
	public final int tag;
	
	public final Node[] childs;
	
	protected Node parent;
	
	final ArrayList<Integer> marks = new ArrayList<>();
	
	public int index = -1;
	
	public String attachment;
	
	public Node(int tag) {
		this.tag = tag;
		childs = new Node[0];
	}
	
	public Node(int tag, Node...childs) {
		this.tag = tag;
		this.childs = childs;
		for(Node item : this.childs)
			item.parent = this;
	}

	public void mark(int value) {
		marks.add(value);
	}
	
	public Node getParent() {
		return parent;
	}
	
	public final boolean isLeaf() {
		return childs.length == 0;
	}
	
	public final boolean isNullary() {
		return childs.length == 0;
	}
	
	public final boolean isUnary() {
		return childs.length == 1;
	}
	
	public final boolean isBinary() {
		return childs.length == 2;
	}
	
	public final boolean isNAry() {
		return childs.length > 2;
	}
	
	public static final String[] NAMES = {
		"+",
		"&&",
		":=",
		":=",
		"&",
		"|",
		"call",
		"::",
		"/",
		"end",
		"==",
		"event",
		"fetch",
		"fetch",
		"fetch",
		"fetch",
		"",
		">",
		">=",
		"",
		"jfalse",
		"jtrue",
		"jump",
		"<",
		"<=",
		"%",
		"*",
		"-",
		"!",
		"!=",
		"||",
		"param",
		"prop",
		"ref",
		"return",
		"",
		"-",
		"super"
	};
	
	public static final int T_ADD = 0;
	public static final int T_AND = 1;
	public static final int T_ASSIGN = 2;
	public static final int T_ASSIGN4 = 3;
	public static final int T_BIT_AND = 4;
	public static final int T_BIT_OR = 5;
	public static final int T_CALL = 6;
	public static final int T_CONCAT = 7;
	public static final int T_DIV = 8;
	public static final int T_END = 9;
	public static final int T_EQUAL = 10;
	public static final int T_EVENT = 11;
	public static final int T_FETCH = 12;
	public static final int T_FETCH4 = 13;
	public static final int T_FETCHF = 14;
	public static final int T_FETCHD = 15;
	public static final int T_FLOAT = 16;
	public static final int T_GREATER = 17;
	public static final int T_GREATER_EQUAL = 18;
	public static final int T_INTEGER = 19;
	public static final int T_JFALSE = 20;
	public static final int T_JTRUE = 21;
	public static final int T_JUMP = 22;
	public static final int T_LESS = 23;
	public static final int T_LESS_EQUAL = 24;
	public static final int T_MOD = 25;
	public static final int T_MUL = 26;
	public static final int T_NEG = 27;
	public static final int T_NOT = 28;
	public static final int T_NOT_EQUAL = 29;
	public static final int T_OR = 30;
	public static final int T_PARAM = 31;
	public static final int T_PROP = 32;
	public static final int T_REF = 33;
	public static final int T_RETURN = 34;
	public static final int T_STRING = 35;
	public static final int T_SUB = 36;
	public static final int T_SUPER = 37;
	
	public static final Node of(int tag) {
		return new Node(tag);
	}
	
	public static final Node of(int tag, Node...args) {
		return new Node(tag, args);
	}
	
	public static final Node integer(int value) {
		return new NInteger(value);
	}
	
	public static final Node of(float value) {
		return new NFloat(value);
	}
	
	public static final Node of(String value) {
		return new NString(value);
	}
	
	public static final Node ref(Node node) {
		return new NRef(node);
	}
	
	public static final Node of(Function func, Node...args) {
		return new NCall(func, args);
	}
}