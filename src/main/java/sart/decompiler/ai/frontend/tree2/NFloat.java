package sart.decompiler.ai.frontend.tree2;

public class NFloat extends Node {
	public final float value;
	
	public NFloat(float value) {
		super(T_FLOAT);
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "" + value;
	}
}
