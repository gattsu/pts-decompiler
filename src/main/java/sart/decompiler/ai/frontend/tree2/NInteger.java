package sart.decompiler.ai.frontend.tree2;

public class NInteger extends Node {
	public final int value;
	
	public NInteger(int value) {
		super(T_INTEGER);
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "" + value;
	}
}
