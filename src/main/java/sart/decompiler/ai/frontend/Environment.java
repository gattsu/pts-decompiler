package sart.decompiler.ai.frontend;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;

import sart.decompiler.ai.frontend.tree2.TreeParser;
import sart.decompiler.ai.interpreter.Clazz;

public class Environment {
	public final HashMap<Integer, Function> functions = new HashMap<>();
	
	public final HashMap<String, Clazz> classes = new HashMap<>();
	
	public Clazz getClass(String name) {
		return classes.get(name);
	}
	
	public void register(Clazz clazz) {
		if(classes.containsKey(clazz.name))
			throw new RuntimeException("class with name " + clazz.name + " already reigstered.");
		classes.put(clazz.name, clazz);
	}
	
	/*
	 * @create если <b>true</b> создает функцию, когда не существует с заданным индексом @index, и возвращает её. 
	 * Используется в случае сканирования сигнатуры
	 */
	public Function getFunction(int index, boolean create) {
		Function func = functions.get(index);
		if(create && func == null)
			functions.put(index, (func = new Function(index, "", false, -1)));
		return func;
	}
	
	public Function getFunction(int index) {
		return getFunction(index, false);
	}
	
	public void storeSignature(String file) {
		final StringBuilder str = new StringBuilder();
		functions.values().forEach(f -> str.append(f.index).append("#").append(f.name).append("#").append(f.ret).append("#").append(f.argc).append("\n"));
		try {
			Files.write(Paths.get(file), str.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		} catch(IOException e) { e.printStackTrace(); }
	}
	
	public void loadSignature(String file) {
		try {
			Files.lines(Paths.get(file)).forEach(line -> {
				String[] arr = line.split("#");
				functions.put(Integer.parseInt(arr[0]), new Function(Integer.parseInt(arr[0]), arr[1], Boolean.parseBoolean(arr[2]), Integer.parseInt(arr[3])));
			});
		} catch(IOException e) { e.printStackTrace(); }
	}

	public void parse() {
		final TreeParser parser = new TreeParser(this);
		classes.values().forEach(cls -> {
			cls.handlers.forEach(h -> {
				parser.parse(h);
				h.ir = h.tree.toString();
			});
		});
	}
}
