package sart.decompiler.ai.frontend;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Function {
	public int index = -1;
	
	public String name = "";
	
	public boolean ret = false;
	
	public int argc = -1;

	public boolean complete = false;
	
	public Function(int index, String name, boolean ret, int argc) {
		this.index = index;
		this.name = name;
		this.ret = ret;
		this.argc = argc;
	}
	
	public final static HashMap<Integer, Function> functions = new HashMap<>();
	
	public static Function get(int index){
		return functions.get(index);
	}
	
	/*
	 * Try get function with @index.
	 * If function not exists, create with @index and @name
	 */
	public static Function get(int index, String name) {
		Function func = functions.get(index);
		if(func == null)
			functions.put(index, (func = new Function(index, name, false, -1)));
		return func;
	}
	
	public static void add(int index, String name, boolean ret, int args) {
		functions.put(index, new Function(index, name, ret, args));
	}
	
	@Override
	public String toString() { return index + "#" + name + "#" + ret + "#" + argc; }
	
	static {
//		add(184680516, "ShowPage", false, 3);
//		add(184680579, "OwnItemCount", true, 3);
//		add(184680487, "AddTimerEx", false, 3);
//		add(184615005, "FloatToInt", true, 2);
//		add(185270503, "CreateOnePrivateEx", false, 12);
//		add(218169344, "Rand", true, 2);
//		add(184811541, "AddEffectActionDesire", false, 5);
//		add(184549553, "Castle_GetPledgeId", true, 1);
//		add(184680582, "FHTML_SetFileName", false, 3);
//		add(184549527, "Castle_GetPledgeName", true, 1);
//		add(184746121, "FHTML_SetStr", false, 4);
//		add(184549528, "Castle_GetOwnerName", true, 1);
//		add(184549531, "Residence_GetTaxRateCurrent", true, 1);
//		add(184746119, "FHTML_SetInt", false, 4);
//		add(184942731, "MakeFString", true, 7);
//		add(184680586, "ShowFHTML", false, 3);
//		add(184549715, "GetSSQStatus", true, 1);
//		add(184615265, "GetSSQPart", true, 2);
//		add(184680529, "ShowMultisell", false, 3);
//		add(184615062, "IsMyLord", true, 2);
	}

	public static void load(String file) {
		try {
			Files.lines(Paths.get(file)).forEach(line -> {
				String[] values = line.split("#");
				add(Integer.parseInt(values[0]), values[1], Boolean.parseBoolean(values[2]), Integer.parseInt(values[3]));
			});
		} catch(IOException e) { e.printStackTrace(); }
	}
	
	public static void store(String file) {
		System.out.println("Storing functions...");
		try {
			new File(file).delete();
			Files.write(Paths.get(file), functions.values().stream().filter(f -> f.complete).map(Function::toString).collect(Collectors.joining("\n")).getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
		} catch(IOException e) { e.printStackTrace(); }
	}

	public static void hands() {
		functions.values().forEach(f -> { if(!f.complete) System.out.println(f); });
	}
}
