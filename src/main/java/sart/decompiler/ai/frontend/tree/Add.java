package sart.decompiler.ai.frontend.tree;

import java.lang.reflect.Field;

import sart.lin.ai.types.NpcEvent;

public class Add extends Expr {
	public final Expr left, right;
	
	public Add(Expr op1, Expr op2) {
		left = op1;
		right = op2;
	}

	public Expr eval() {
		if(left instanceof Event && right instanceof IConst) {
			Field field = NpcEvent.find(((IConst)right).value);
			return new GetField(field.getName(), field);
		} else
			return this;
	}
	
	public String toString() {
		return left + " + " + right;
	}
	
	public void print(StringBuilder str) {
		left.print(str);
		right.print(str);
		print_op(str, "+", left, right);
	}
}
