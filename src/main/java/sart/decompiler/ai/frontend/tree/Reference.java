package sart.decompiler.ai.frontend.tree;

public class Reference extends Expr{
	public final Expr expr;
	
	public Reference(Expr expr) {
		this.expr = expr;
	}
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append(expr.token()).append("\n");
	}
	
}
