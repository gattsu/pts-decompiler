package sart.decompiler.ai.frontend.tree;

public class Fetch extends Expr {
	public final Expr expr;
	
	public Fetch(Expr expr) {
		this.expr = expr;
	}

	@Override
	public Expr eval() {
		return expr.eval();
	}
	
	@Override
	public void print(StringBuilder str) {
		expr.print(str);
		str.append(token()).append(" = ").append("fetch ").append(expr.token()).append('\n');
	}
}
