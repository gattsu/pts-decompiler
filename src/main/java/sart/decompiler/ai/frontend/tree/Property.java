package sart.decompiler.ai.frontend.tree;

public class Property extends Expr {
	public final String name;
	
	public Property(String name) {
		this.name = name;
	}
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("prom ").append(name).append("\n");
	}
}
