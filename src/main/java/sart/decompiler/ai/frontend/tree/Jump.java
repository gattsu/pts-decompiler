package sart.decompiler.ai.frontend.tree;

public class Jump extends Expr {
	public int label;
	
	public Jump(int label) {
		this.label = label;
	}

	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("jump ").append(label).append("\n");
	}
	
	@Override
	public String toString() {
		return "jump " + label; 
	}
}
