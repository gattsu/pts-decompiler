package sart.decompiler.ai.frontend.tree;

public class Return extends Expr {

	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = return\n");
	}
	
	@Override
	public String toString() {
		return "return";
	}
	
}
