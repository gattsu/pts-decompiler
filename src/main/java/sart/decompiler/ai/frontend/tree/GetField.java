package sart.decompiler.ai.frontend.tree;

import java.lang.reflect.Field;

import sart.lin.ai.types.annotation.offset;

public class GetField extends Expr {
	public final String name;
	
	public final Field field;
	
	public GetField(String name, Field field) {
		this.name = name;
		this.field = field;
	}
	
	public Expr getField(int index) {
		for(Field field : field.getType().getDeclaredFields()) 
			if(field.isAnnotationPresent(offset.class) && field.getAnnotation(offset.class).value() == index)
				return new GetField(name + "." + field.getName(), field);
		throw new RuntimeException("not found field " + index + " " + field.getType());
		
		
	}
	
	@Override
	public String toString() {
		return name;
	}
}
