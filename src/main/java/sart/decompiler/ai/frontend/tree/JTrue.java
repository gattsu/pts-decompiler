package sart.decompiler.ai.frontend.tree;

public class JTrue extends Expr {
	public final int label;
	
	public JTrue(int label) {
		this.label = label;
	}

	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("jfalse ").append(label).append("\n");
	}
	
	@Override
	public String toString() {
		return "jtrue " + label;
	}
}
