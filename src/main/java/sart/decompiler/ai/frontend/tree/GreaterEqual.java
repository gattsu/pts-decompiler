package sart.decompiler.ai.frontend.tree;

public class GreaterEqual extends Expr {
	public final Expr left, right;
	
	public GreaterEqual(Expr op1, Expr op2) {
		left = op1;
		right = op2;
	}

	public void print(StringBuilder str) {
		left.print(str);
		right.print(str);
		print_op(str, ">=", left, right);
	}
}
