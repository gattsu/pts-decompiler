package sart.decompiler.ai.frontend.tree;

import java.util.ArrayList;

public abstract class Expr {
	public ArrayList<Integer> labels = new ArrayList<>();
	
	public int index;

	public Expr eval() { return this; }
	
	public void print(StringBuilder str) {}
	
	public void print_op(StringBuilder str, String op, Expr left, Expr right) {
		str.append(token()).append(" = ").append(left.token()).append(" ").append(op).append(" ").append(right.token()).append("\n");
	}
	
	public void capture() {}
	
	public final String token() { return "t" + index; }
	
	public void mark(int label) { labels.add(label); }
	
	public boolean isTerminal() {
		return this instanceof Assign || this instanceof JFalse || this instanceof JTrue || this instanceof JFalse;
	}
}
