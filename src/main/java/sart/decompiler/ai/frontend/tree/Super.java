package sart.decompiler.ai.frontend.tree;

public final class Super extends Expr {
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = super\n");
	}
}
