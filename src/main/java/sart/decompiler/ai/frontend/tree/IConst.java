package sart.decompiler.ai.frontend.tree;

public class IConst extends Expr {
	public final int value;
	
	public IConst(int value) {
		this.value = value;
	}
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append(value).append('\n');
	}
	
	@Override
	public String toString() {
		return "" + value;
	}
}
