package sart.decompiler.ai.frontend.tree;

public class Param extends Expr {
	public final String name;
	
	public Param(String name) {
		this.name = name;
	}
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("param ").append(name).append("\n");
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
