package sart.decompiler.ai.frontend.tree;

public class Neg extends Expr {
	public final Expr op;
	
	public Neg(Expr op) {
		this.op = op;
	}

	public void print(StringBuilder str) {
		op.print(str);
		str.append(token()).append(" = ").append("-").append(op.token()).append("\n");
	}
}
