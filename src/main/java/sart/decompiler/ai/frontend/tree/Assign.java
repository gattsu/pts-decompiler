package sart.decompiler.ai.frontend.tree;

public class Assign extends Expr {
	public final Expr left, right;
	
	public Assign(Expr op1, Expr op2) {
		left = op1;
		right = op2;
	}

	public void print(StringBuilder str) {
		left.print(str);
		right.print(str);
		print_op(str, ":=", left, right);
	}
}
