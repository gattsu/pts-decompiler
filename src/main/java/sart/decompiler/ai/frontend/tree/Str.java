package sart.decompiler.ai.frontend.tree;

public class Str extends Expr {
	public final String value;
	
	public Str(String value) {
		this.value = value;
	}
	
	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = \"").append(value).append("\"\n");
	}
	
	@Override
	public String toString() {
		return '"' + value + '"';
	}
}
