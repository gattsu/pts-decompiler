package sart.decompiler.ai.frontend.tree;

import java.util.ArrayList;

public final class ParseTree {
	private final ArrayList<Expr> list = new ArrayList<>();
	
	public void add(Expr expr) {
		list.add(expr);
	}
	
	@Override
	public String toString() {
		final StringBuilder str = new StringBuilder();
		int index = 0;
		for(Expr expr : list)
			str.append("t").append(index++).append(" = ").append(expr.eval()).append("\n");
		return str.toString();
	}
	
}
