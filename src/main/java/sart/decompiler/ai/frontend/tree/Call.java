package sart.decompiler.ai.frontend.tree;

import java.util.Arrays;
import java.util.stream.Collectors;

import sart.decompiler.ai.frontend.Function;

public class Call extends Expr {
	public final Function func;
	
	public final Expr[] args;
	
	public Call(Function func, Expr[] args) {
		this.func = func;
		this.args = args;
	}
	
	public void print(StringBuilder str) {
		Arrays.stream(args).forEach(item -> item.print(str));
	//	str.append(token()).append(" = call ").append(func.name).append(Arrays.stream(args).map(Object::toString).collect(Collectors.joining(", ", "(", ")")).append("\n");
	}
	
	@Override
	public boolean isTerminal() {
		return !func.ret;
	}
	
	/*
	 * Если функцию захватывают, как часть выражения, значит, данная функция, возвращает значение.
	 */
	@Override
	public void capture() { func.ret = true; }
	
	@Override
	public String toString() {
		return "call " + func.name + " " + Arrays.stream(args).map(Expr::eval).map(Object::toString).collect(Collectors.joining(", ", "(", ")"));
	}
}
