package sart.decompiler.ai.frontend.tree;

public class JFalse extends Expr {
	public final int label;
	
	public JFalse(int label) {
		this.label = label;
	}

	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("jfalse ").append(label).append("\n");
	}
	
	@Override
	public String toString() {
		return "jfalse " + label;
	}
}
