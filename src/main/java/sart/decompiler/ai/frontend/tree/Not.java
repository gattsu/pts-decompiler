package sart.decompiler.ai.frontend.tree;

public class Not extends Expr {
	public final Expr op;
	
	public Not(Expr op) {
		this.op = op;
	}

	public void print(StringBuilder str) {
		op.print(str);
		str.append(token()).append(" = ").append("!").append(op.token()).append("\n");
	}
}
