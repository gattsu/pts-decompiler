package sart.decompiler.ai.frontend.tree;

public class FConst extends Expr {
	public final float value;
	
	public FConst(float value) {
		this.value = value;
	}

	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append(value).append("\n");
	}
}
