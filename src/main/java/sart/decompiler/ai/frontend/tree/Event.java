package sart.decompiler.ai.frontend.tree;

/*
 * Special event value 
 */
public final class Event extends Expr {

	@Override
	public void print(StringBuilder str) {
		str.append(token()).append(" = ").append("event\n");
	}
}
