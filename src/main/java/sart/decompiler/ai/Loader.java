package sart.decompiler.ai;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.frontend.lexer.Lexer;
import sart.decompiler.ai.frontend.lexer.Token;
import sart.decompiler.ai.frontend.lexer.Token.ETag;
import sart.decompiler.ai.interpreter.Clazz;
import sart.decompiler.ai.interpreter.Instruction;

public class Loader {
	public final HashMap<String, String> header = new HashMap<>();
	
	public final HashMap<String, Clazz> classes = new HashMap<>();
	
	public final ArrayList<String> loadingOrder = new ArrayList<>();
	
	public final Environment env;
	
	public Loader() { env = null; }
	
	public Loader(Environment env) {
		this.env = env;
	}
	
	public void load(String file) {
		header.clear();
		loadingOrder.clear();
		try(Lexer lexer = new Lexer(file)) {
			parse(lexer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void parse(Lexer lexer) throws IOException {
		Token token = null;
		while(!(token = lexer.next()).is(ETag.EOS)) {
			if(!token.is(ETag.WORD))
				throw new IOException("syntax error: waiting word tag " + token + " " + lexer.line);
			switch(token.value) {
			case "SizeofPointer": header.put(token.value, lexer.next().value); break;//TODO check
			case "SharedFactoryVersion": header.put(token.value, lexer.next().value); break;//TODO check
			case "NPCHVersion": header.put(token.value, lexer.next().value); break;//TODO check
			case "NASCVersion": header.put(token.value, lexer.next().value); break;//TODO check
			case "NPCEventHVersion": header.put(token.value, lexer.next().value); break;//TODO check
			case "Debug": header.put(token.value, lexer.next().value); break;//TODO check
			case "class": parseClass(lexer); break;
			default:
				throw new IOException("unexcepted token " + token);
			}
		}
	}
	
	private Clazz clazz = null;
	
	public Clazz get(String name) {
		return env.getClass(name);
	}
	
	private void parseClass(Lexer lexer) throws IOException {
		Token token = null;
		clazz = null;
		//branch
		int branch = lexer.next().asInteger();//TODO check ETag.INT
		String name = lexer.next().asString();//TODO check ETag.WORD
		lexer.next();//TODO check ETag ;
		Clazz parent = null;
		if((token = lexer.next()).is(ETag.LBRACKET)) {
			lexer.next();//TODO check null
			lexer.next();//TODO check )
		} else {//TODO check is token.is(ETag.WORD)
			parent = env.getClass(token.value);
			if(parent == null)
				throw new IOException("not found parent class " + token.value + " for " + name + " " + lexer.line);
		}
		clazz = new Clazz(name, branch, parent);
		while(!(token = lexer.next()).is("class_end")) {
			switch(token.value) {
			case "parameter_define_begin": parseParameter(lexer); break;
			case "property_define_begin": parseProperty(lexer); break;
			case "handler": parseHandler(lexer); break;
			default:
				throw new IOException("unexpected token " + token + " " + lexer.line);
			}
		}
		env.register(clazz);
		loadingOrder.add(name);
		//classes.put(name, clazz);
	}
	
	private void parseProperty(Lexer lexer) throws IOException {
		while(!lexer.next().is("property_define_end"));//TODO
	}
	
	private void parseParameter(Lexer lexer) throws IOException {
		while(!lexer.next().is("parameter_define_end"));//TODO
	}
	
	private final ArrayList<String> strings = new ArrayList<>();
	
	private final ArrayList<String> variables = new ArrayList<>();

	private final HashMap<Integer, String> stringval = new HashMap<>();
	
	private final ArrayList<Instruction> code = new ArrayList<>();
	
	public void parseHandler(Lexer lexer) throws IOException {
		strings.clear();
		variables.clear();
		stringval.clear();
		code.clear();
		int id = lexer.next().asInteger();
		int lines = lexer.next().asInteger();
		lexer.next();//handler name . comment.
		Token token = null;
		if(!lexer.next().is("variable_begin"))
			throw new IOException("syntax error: expect variable_begin " + lexer.prev() + " line " + lexer.line);
		parseVariable(lexer);
		while(!(token = lexer.next()).is("handler_end")) {
			if(token.is(ETag.COMMENT))
				continue;
			if(token.value.startsWith("S")) {
				int index = Integer.parseInt(token.asString().substring(1));
				lexer.next();//.
				stringval.put(index, lexer.next().asString());
				code.add(new Instruction(Instruction.STRING, index, null));
				continue;
			}
			if(token.value.startsWith("L")) {
				int index = Integer.parseInt(token.asString().substring(1));
				code.add(new Instruction(Instruction.LABEL, index, null));
				continue;
			}
			switch(token.value) {
			case "add":
				code.add(new Instruction(Instruction.ADD, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "add_string":
				code.add(new Instruction(Instruction.ADD_STRING, lexer.next().asInteger(), lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "and":
				code.add(new Instruction(Instruction.AND, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "assign4":
				code.add(new Instruction(Instruction.ASSIGN4, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "assign":
				code.add(new Instruction(Instruction.ASSIGN, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "bit_and":
				code.add(new Instruction(Instruction.BIT_AND, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "bit_or":
				code.add(new Instruction(Instruction.BIT_OR, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "branch_true": {
				int label = Integer.parseInt(lexer.next().asString().substring(1));
				code.add(new Instruction(Instruction.BRANCH_TRUE, label, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "branch_false": {
				int label = Integer.parseInt(lexer.next().asString().substring(1));
				code.add(new Instruction(Instruction.BRANCH_FALSE, label, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "call_super":
				code.add(new Instruction(Instruction.CALL_SUPER, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "div":
				code.add(new Instruction(Instruction.DIV, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "equal":
				code.add(new Instruction(Instruction.EQUAL, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "exit_handler":
				code.add(new Instruction(Instruction.EXIT_HANDLER, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "fetch_d":
				code.add(new Instruction(Instruction.FETCH_D, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "fetch_f":
				code.add(new Instruction(Instruction.FETCH_F, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "fetch_i":
				code.add(new Instruction(Instruction.FETCH_I, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "fetch_i4":
				code.add(new Instruction(Instruction.FETCH_I4, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "func_call":
				code.add(new Instruction(Instruction.FUNC_CALL, lexer.next().asInteger(), lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "greater":
				code.add(new Instruction(Instruction.GREATER, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "greater_equal":
				code.add(new Instruction(Instruction.GREATER_EQUAL, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "jump": {
				int label = Integer.parseInt(lexer.next().asString().substring(1));
				code.add(new Instruction(Instruction.JUMP, label, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "less":
				code.add(new Instruction(Instruction.LESS, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "less_equal":
				code.add(new Instruction(Instruction.LESS_EQUAL, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "mod":
				code.add(new Instruction(Instruction.MOD, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "mul":
				code.add(new Instruction(Instruction.MUL, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "negate":
				code.add(new Instruction(Instruction.NEGATE, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "not":
				code.add(new Instruction(Instruction.NOT, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "not_equal":
				code.add(new Instruction(Instruction.NOT_EQUAL, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "or":
				code.add(new Instruction(Instruction.OR, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "push_const": {
				if(lexer.next(0).is(ETag.INT)) {
					code.add(new Instruction(Instruction.PUSH_CONST, lexer.next().asInteger(), lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				} else {
					code.add(new Instruction(Instruction.PUSH_CONST_F, Float.floatToIntBits(lexer.next().asFloat()), lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				}
				break;
			}
			case "push_event":
				code.add(new Instruction(Instruction.PUSH_EVENT, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "push_parameter": {
				int index = strings.size();
				strings.add(lexer.next().asString());
				code.add(new Instruction(Instruction.PUSH_PARAMETER, index, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "push_property": {
				int index = strings.size();
				strings.add(lexer.next().asString());
				code.add(new Instruction(Instruction.PUSH_PROPERTY, index, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "push_reg_sp":
				code.add(new Instruction(Instruction.PUSH_REG_SP, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "push_string": {
				int index = Integer.parseInt(lexer.next().asString().substring(1));
				code.add(new Instruction(Instruction.PUSH_STRING, index, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			}
			case "shift_sp":
				code.add(new Instruction(Instruction.SHIFT_SP, lexer.next().asInteger(), lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			case "sub":
				code.add(new Instruction(Instruction.SUB, lexer.next(0).is(ETag.COMMENT) ? lexer.next().asString() : null));
				break;
			default:
				throw new IOException("unexpected token " + token + " " + lexer.line + " " + clazz.name);
			}
		}
		clazz.addHandler(id, lines, variables.toArray(new String[variables.size()]), new HashMap<>(stringval), strings.toArray(new String[strings.size()]), code.toArray(new Instruction[code.size()]));
	}
	
	private void parseVariable(Lexer lexer) throws IOException {
		Token token = null;
		while(!(token = lexer.next()).is("variable_end"))
			variables.add(token.asString());
	}


}
