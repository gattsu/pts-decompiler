package sart.decompiler.ai;

public class Main {

	public static void main(String...args) {
		Loader loader = new Loader();
		loader.load("ai.obj");
		final Parser parser = new Parser();
		loader.classes.forEach((name, clazz) -> {
			clazz.handlers.forEach(parser::execute);
		});
	}
	
}
