package sart.decompiler.ai;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import sart.decompiler.ai.frontend.Environment;
import sart.decompiler.ai.interpreter.Clazz;
import sart.decompiler.ai.interpreter.Handler;

public class SingleTest {
	public static final Path path = Paths.get("ir");
	
	public static void main(String...args) {
		final Environment env = new Environment();
		env.loadSignature("signature");
		final Loader loader = new Loader(env);
		loader.load("classes/d/default_npc");
		loader.load("classes/c/citizen");
		env.parse();
		env.classes.values().forEach(SingleTest::store);
	}
	
	public static void store(Clazz cls) {
		final StringBuilder str = new StringBuilder();
		str.append("class ").append(cls.name);
		if(cls.parent != null)
			str.append(" : ").append(cls.parent.name);
		str.append(" {\n\n");
		for(Handler handler : cls.handlers)
			str.append("handler ").append(handler.getName()).append("\n").append(handler.ir).append("handler_end\n\n");
		str.append("}\n");
		final Path dir = path.resolve(""  + cls.name.charAt(0));
		try {
			if(Files.notExists(dir))
				Files.createDirectories(dir);
			flush(dir.resolve(cls.name), str);
		} catch(IOException e) { e.printStackTrace(); }
	}
	
	public static void flush(Path file, StringBuilder str) throws IOException {
		Files.write(file, str.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
	}
}
