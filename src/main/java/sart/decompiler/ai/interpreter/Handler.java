package sart.decompiler.ai.interpreter;

import java.util.HashMap;

import sart.decompiler.ai.frontend.tree2.TreeNode;

import static sart.decompiler.ai.interpreter.Instruction.*;

public final class Handler {
	public final Clazz clazz;
	
	public final int id;
	
	public final int lines;

	public final String[] variables;
	
	public final HashMap<Integer, String> strmap;
	
	public final String[] strings;
	
	public final Instruction[] code;
	
	public String attachment;

	public TreeNode tree;
	
	public String ir;
	
	public Handler(Clazz clazz, int id, int lines, String[] variables, HashMap<Integer, String> strmap, String[] strings, Instruction[] code) {
		this.clazz = clazz;
		this.id = id;
		this.lines = lines;
		this.variables = variables;
		this.strmap = strmap;
		this.strings = strings;
		this.code = code;
	}
	
	public String toString() {
		final StringBuilder str = new StringBuilder();	
		str.append("handler ").append(id).append(" ").append(lines).append("\t//  ").append(getName()).append("\n");
		str.append("\tvariable_begin\n");
		for(String var : variables)
			str.append("\t\t\"").append(var).append("\"\n");
		str.append("\tvariable_end\n\n");
		for(Instruction inst : code) {
			if(inst.opcode == Instruction.LABEL) {
				str.append("L").append(inst.operand).append("\n");
				continue;
			}
			if(inst.opcode == Instruction.STRING) {
				str.append("S").append(inst.operand).append(". ").append('"').append(strmap.get(inst.operand)).append('"').append("\n");
				continue;
			}
			if(inst.opcode == Instruction.PUSH_CONST_F) {
				str.append("\t").append("push_const ").append(Float.intBitsToFloat(inst.operand)).append("\n");
				continue;
			}
			str.append("\t").append(Instruction.OP_NAMES[inst.opcode]);
			switch(inst.opcode) {
			case PUSH_CONST:
			case SHIFT_SP:
			case FUNC_CALL:
			case ADD_STRING:
				str.append(" ").append(inst.operand);
				break;
			case BRANCH_FALSE:
			case BRANCH_TRUE:
			case JUMP:
				str.append(" L").append(inst.operand);
				break;
			case PUSH_STRING:
				str.append(" S").append(inst.operand);
				break;
			case PUSH_PARAMETER:
			case PUSH_PROPERTY:
				str.append(" ").append(strings[inst.operand]);
				break;
			}
			if(inst.comment != null)
				str.append("\t//").append(inst.comment);
			str.append("\n");
		}
		str.append("handler_end\n\n");
		return str.toString();
	}
	
	public String getName() {
		if(clazz.branch == 1) 
			return NPC_HANDLER_HAME[id];
		else
			return MAKER_HANDLER_NAME[id];
	}
	
	public final static String[] NPC_HANDLER_HAME = {
		"NO_DESIRE",//0
		"ATTACKED",//1
		"SPELLED",//2
		"TALKED",//3
		"TALK_SELECTED",//4
		"SEE_CREATURE",//5
		"NOT_DEFINED_HANDLER_6",//6
		"NOT_DEFINED_HANDLER_7",//7
		"NOT_DEFINED_HANDLER_8",//8
		"NOT_DEFINED_HANDLER_9",//9
		"MY_DYING",//10
		"TIMER_FIRED",//11
		"TIMER_FIRED_EX",//12
		"CREATED",//13
		"NOT_DEFINED_HANDLER_14",//14
		"NOT_DEFINED_HANDLER_15",//15
		"SEE_SPELL",//16
		"OUT_OF_TERRITORY",//17
		"DESIRE_MANIPULATION",//18
		"PARTY_ATTACKED",//19
		"NOT_DEFINED_HANDLER_20",//20
		"PARTY_DIED",//21
		"NOT_DEFINED_HANDLER_22",//22
		"CLAN_ATTACKED",//23
		"NOT_DEFINED_HANDLER_24",//24
		"STATIC_OBJECT_CLAN_ATTACKED",//25
		"NOT_DEFINED_HANDLER_26",//26
		"NOT_DEFINED_HANDLER_27",//27
		"TELEPORT_REQUESTED",//28
		"NOT_DEFINED_HANDLER_29",//29
		"NOT_DEFINED_HANDLER_30",//30
		"QUEST_ACCEPTED",//31
		"MENU_SELECTED",//32
		"LEARN_SKILL_REQUESTED",//33
		"ENCHANT_SKILL_REQUESTED",//34
		"ONE_SKILL_SELECTED",//35
		"ONE_ENCHANT_SKILL_SELECTED",//36
		"NOT_DEFINED_HANDLER_37",//37
		"CLASS_CHANGE_REQUESTED",//38
		"MANOR_MENU_SELECTED",//39
		"NOT_DEFINED_HANDLER_40",//40
		"NOT_DEFINED_HANDLER_41",//41
		"NOT_DEFINED_HANDLER_42",//42
		"NOT_DEFINED_HANDLER_43",//43
		"NOT_DEFINED_HANDLER_44",//44
		"CREATE_PLEDGE",//45
		"DISMISS_PLEDGE",//46
		"REVIVE_PLEDGE",//47
		"LEVEL_UP_PLEDGE",//48
		"NOT_DEFINED_HANDLER_49",//49
		"NOT_DEFINED_HANDLER_50",//50
		"CREATE_ALLIANCE",//51
		"SCRIPT_EVENT",//52
		"TUTORIAL_EVENT",//53
		"QUESTION_MARK_CLICKED",//54
		"USER_CONNECTED",//55
		"NOT_DEFINED_HANDLER_56",//56
		"NOT_DEFINED_HANDLER_57",//57
		"ATTACK_FINISHED",//58
		"NOT_DEFINED_HANDLER_59",//59
		"NOT_DEFINED_HANDLER_60",//60
		"NOT_DEFINED_HANDLER_61",//61
		"NOT_DEFINED_HANDLER_62",//62
		"NOT_DEFINED_HANDLER_63",//63
		"MOVE_TO_WAY_POINT_FINISHED",//64
		"USE_SKILL_FINISHED",//65
		"MOVE_TO_FINISHED",//66
		"NOT_DEFINED_HANDLER_67",//67
		"NOT_DEFINED_HANDLER_68",//68
		"NOT_DEFINED_HANDLER_69",//69
		"NOT_DEFINED_HANDLER_70",//70
		"NOT_DEFINED_HANDLER_71",//71
		"NOT_DEFINED_HANDLER_72",//72
		"NOT_DEFINED_HANDLER_73",//73
		"NOT_DEFINED_HANDLER_74",//74
		"NOT_DEFINED_HANDLER_75",//75
		"NOT_DEFINED_HANDLER_76",//76
		"NOT_DEFINED_HANDLER_77",//77
		"DOOR_HP_LEVEL_INFORMED",//78
		"CONTROLTOWER_LEVEL_INFORMED",//79
		"TB_REGISTER_PLEDGE_RETURNED",//80
		"TB_REGISTER_MEMBER_RETURNED",//81
		"TB_GET_NPC_TYPE_INFORMED",//82
		"TB_SET_NPC_TYPE_RETURNED",//83
		"TB_GET_PLEDGE_REGISTER_STATUS_INFORMED",//84
		"TB_GET_BATTLE_ROYAL_PLEDGE_LIST_INFORMED",//85
		"SUBJOB_LIST_INFORMED",//86
		"SUBJOB_CREATED",//87
		"SUBJOB_CHANGED",//88
		"SUBJOB_RENEWED",//89
		"ON_SSQ_SYSTEM_EVENT",//90
		"SET_AGIT_DECO_RETURNED",//91
		"RESET_AGIT_DECO_RETURNED",//92
		"CLAN_DIED",//93
		"NOT_DEFINED_HANDLER_94",//94
		"SET_HERO_RETURNED",//95
		"DELETE_PREVIOUS_OLYMPIAD_POINT_RETURNED",//96
	};
	
	public final static String[] MAKER_HANDLER_NAME = {
		"ON_START",//0
		"ON_NPC_DELETED",//1
		"ON_SCRIPT_EVENT",//2
		"ON_DB_NPC_INFO",//3
		"ON_TIMER",//4
		"ON_NPC_CREATED"//5
	};
}
