package sart.decompiler.ai.interpreter;

import static sart.decompiler.ai.interpreter.Instruction.*;

public class Interpreter {
	private Handler handler;
	
	private Instruction[] code;
	
	private Instruction inst;
	
	private int ip;

	public void attach(String code) {
		handler.ir = code;
	}
	/**
	 * Return current line number of execution instruction
	 * @return execution line number
	 */
	public final int getLineNum() {
		return ip;
	}
	
	/**
	 * Return commentary of current instruction if exists, otherwise return null.
	 * @return instruction commentary
	 */
	public final String getComment() {
		return inst.comment;
	}

	public final String getString(int index) {
		return handler.strings[index];
	}
	
	public final String getMapString(int index) {
		return handler.strmap.get(index);
	}
	
	public final void execute(Handler handler) {
		ip = 0;
		this.handler = handler;
		code = handler.code;
		onStart();
		while(ip < code.length) {
			inst = code[ip];
			switch(inst.opcode) {
			case ADD: ex_add(); break;
			case ADD_STRING: ex_add_string(inst.operand); break;
			case AND: ex_and(); break;
			case ASSIGN4: ex_assign4(); break;
			case ASSIGN: ex_assign(); break;
			case BIT_AND: ex_bit_and(); break;
			case BIT_OR: ex_bit_or(); break;
			case BRANCH_TRUE: ex_branch_true(inst.operand); break;
			case BRANCH_FALSE: ex_branch_false(inst.operand); break;
			case CALL_SUPER: ex_call_super(); break;
			case DIV: ex_div(); break;
			case EQUAL: ex_equal(); break;
			case EXIT_HANDLER: ex_exit_handler(); break;
			case FETCH_D: ex_fetch_d(); break;
			case FETCH_F: ex_fetch_f(); break;
			case FETCH_I: ex_fetch_i(); break;
			case FETCH_I4: ex_fetch_i4(); break;
			case FUNC_CALL: ex_func_call(inst.operand); break;
			case GREATER: ex_greater(); break;
			case GREATER_EQUAL: ex_greater_equal(); break;
			case JUMP: ex_jump(inst.operand); break;
			case LESS: ex_less(); break;
			case LESS_EQUAL: ex_less_equal(); break;
			case MOD: ex_mod(); break;
			case MUL: ex_mul(); break;
			case NEGATE: ex_negate(); break;
			case NOT: ex_not(); break;
			case NOT_EQUAL: ex_not_equal(); break;
			case OR: ex_or(); break;
			case PUSH_CONST: ex_push_const(inst.operand); break;
			case PUSH_EVENT: ex_push_event(); break;
			case PUSH_PARAMETER: ex_push_parameter(inst.operand); break;
			case PUSH_PROPERTY: ex_push_property(inst.operand); break;
			case PUSH_REG_SP: ex_push_reg_sp(); break;
			case PUSH_STRING: ex_push_string(inst.operand); break;
			case SHIFT_SP: ex_shift_sp(inst.operand); break;
			case SUB: ex_sub(); break;
			case PUSH_CONST_F: ex_push_const_f(Float.intBitsToFloat(inst.operand));
			case LABEL: ex_label(inst.opcode); break;
			}
			ip++;
		}
		onFinish();
	}
	
	public void onStart() {}
	
	public void onFinish() {}
	
	public void ex_add() {}
	
	public void ex_add_string(int index) {}
	
	public void ex_and() {}
	
	public void ex_assign() {}
	
	public void ex_assign4() {}
	
	public void ex_bit_and() {}
	
	public void ex_bit_or() {}
	
	public void ex_branch_true(int label) {}
	
	public void ex_branch_false(int label) {}
	
	public void ex_call_super() {}
	
	public void ex_div() {}
	
	public void ex_equal() {}
	
	public void ex_exit_handler() {}
	
	public void ex_fetch_d() {}
	
	public void ex_fetch_f() {}
	
	public void ex_fetch_i() {}
	
	public void ex_fetch_i4() {}
	
	public void ex_func_call(int index) {}
	
	public void ex_greater() {}
	
	public void ex_greater_equal() {}
	
	public void ex_jump(int label) {}
	
	public void ex_less() {}
	
	public void ex_less_equal() {}
	
	public void ex_mod() {}
	
	public void ex_mul() {}
	
	public void ex_negate() {}
	
	public void ex_not() {}
	
	public void ex_not_equal() {}
	
	public void ex_or() {}
	
	public void ex_push_const(int value) {}
	
	public void ex_push_event() {}
	
	public void ex_push_parameter(int index) {}
	
	public void ex_push_property(int index) {}
	
	public void ex_push_reg_sp() {}
	
	public void ex_push_string(int index) {}
	
	public void ex_shift_sp(int value) {}
	
	public void ex_sub() {}
	
	public void ex_push_const_f(float value) {}

	public void ex_label(int opcode) { }
	
	//help function
	//get next instruction with offset for current postition
	public Instruction next(int offset) {
		if(ip + offset >= code.length)
			return null;
		return code[ip + offset];
	}
	
	public Instruction prev(int offset) {
		if(ip - offset < 0)
			return null;
		return code[ip - offset];
	}
	
	public String trace() {
		return handler.clazz.name + ":" + handler.getName() + "#" + ip;
	}
}