package sart.decompiler.ai.interpreter;

public final class Instruction {
	public final int opcode;
	
	public final int operand;
	
	public final String comment;
	
	public Instruction(int opcode, String comment) {
		this(opcode, -1, comment);
	}
	
	public Instruction(int opcode, int operand, String comment) {
		this.opcode = opcode;
		this.operand = operand;
		this.comment = comment;
	}
	
	//Check instruction opcode
	public boolean is(int...opcodes) {
		for(int op : opcodes)
			if(op == opcode)
				return true;
		return false;
	}
	
	public boolean not(int...opcodes) {
		for(int value : opcodes)
			if(opcode == value)
				return false;
		return true;
	}
	
	public static final int ADD = 0;
	public static final int ADD_STRING = 1;
	public static final int AND = 2;
	public static final int ASSIGN4 = 3;
	public static final int ASSIGN = 4;
	public static final int BIT_AND = 5;
	public static final int BIT_OR = 6;
	public static final int BRANCH_TRUE = 7;
	public static final int BRANCH_FALSE = 8;
	public static final int CALL_SUPER = 9;
	public static final int DIV = 10;
	public static final int EQUAL = 11;
	public static final int EXIT_HANDLER = 12;
	public static final int FETCH_D = 13;
	public static final int FETCH_F = 14;
	public static final int FETCH_I = 15;
	public static final int FETCH_I4 = 16;
	public static final int FUNC_CALL = 17;
	public static final int GREATER = 18;
	public static final int GREATER_EQUAL = 19;
	public static final int JUMP = 20;
	public static final int LESS = 21;
	public static final int LESS_EQUAL = 22;
	public static final int MOD = 23;
	public static final int MUL = 24;
	public static final int NEGATE = 25;
	public static final int NOT = 26;
	public static final int NOT_EQUAL = 27;
	public static final int OR = 28;
	public static final int PUSH_CONST = 29;
	public static final int PUSH_EVENT = 30;
	public static final int PUSH_PARAMETER = 31;
	public static final int PUSH_PROPERTY = 32;
	public static final int PUSH_REG_SP = 33;
	public static final int PUSH_STRING = 34;
	public static final int SHIFT_SP = 35;
	public static final int SUB = 36;
	public static final int STRING = 37;
	public static final int LABEL = 38;
	public static final int PUSH_CONST_F = 39;
	
	public static final String[] OP_NAMES = {
		"add",
		"add_string",
		"and",
		"assign4",
		"assign",
		"bit_and",
		"bit_or",
		"branch_true",
		"branch_false",
		"call_super",
		"div",
		"equal",
		"exit_handler",
		"fetch_d",
		"fetch_f",
		"fetch_i",
		"fetch_i4",
		"func_call",
		"greater",
		"greater_equal",
		"jump",
		"less",
		"less_equal",
		"mod",
		"mul",
		"negate",
		"not",
		"not_equal",
		"or",
		"push_const",
		"push_event",
		"push_parameter",
		"push_property",
		"push_reg_sp",
		"push_string",
		"shift_sp",
		"sub",
		"S",
		"L",
		"push_const_f"
	};
}