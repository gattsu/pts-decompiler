package sart.decompiler.ai.interpreter;

import java.util.ArrayList;
import java.util.HashMap;

//Ai class
public final class Clazz {
	public final Clazz parent;
	
	public final String name;
	
	public final int branch;
	
	public final ArrayList<Field> fields = new ArrayList<>();
	
	public final ArrayList<Handler> handlers = new ArrayList<>();
	
	public Clazz(String name, int branch, Clazz parent) {
		this.name = name;
		this.parent = parent;
		this.branch = branch;
	}
	
	public void addField(String type, String name, String value) {
		fields.add(new Field(type, name, value));
	}
	
	public class Field {
		public final String type;
		public final String name;
		public final String value;
		
		public Field(String type, String name, String value) {
			this.type = type;
			this.name = name;
			this.value = value;
		}
	}

	public void addHandler(int id, int lines, String[] variables, HashMap<Integer, String> strmap, String[] strings, Instruction[] code) {
		handlers.add(new Handler(this, id, lines, variables, strmap, strings, code));
	}
}
