package sart.decompiler.ai.interpreter;

public final class Code {
	public final Instruction[] code;
	
	public final String[] strings;
	
	public Code(Instruction[] code, String[] strings) {
		this.strings = strings;
		this.code = code;
	}
}