package sart.decompiler.ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import sart.decompiler.ai.frontend.Function;
import sart.decompiler.ai.frontend.tree.*;
import sart.decompiler.ai.interpreter.Instruction;
import sart.decompiler.ai.interpreter.Interpreter;

/*
 * Remove labels and strings from code flow
 * SyntaxError
 * 
 */
public class Parser extends Interpreter {
	public Expr[] stack = new Expr[65536];
	
	public int sp = 0;//start pointer
	
	public int index = 0;
	
	//scan function signture
	public boolean scansig = false;
	
	public ArrayList<Integer> labels = new ArrayList<>();
	
	public ParseTree tree = new ParseTree();
	
	public void push(Expr expr) {
		proces(expr);
		expr.index = index++;
		stack[sp++] = expr;
	}
	
	public Expr pop() {
		if(sp == 0)
			throw new StackOverflowError();
		Expr result = stack[--sp];
		result.capture();
		stack[sp] = null;
		return result;
	}
	
	public void onStart() {
		tree = new ParseTree();
		//System.out.println(trace());
		Arrays.fill(stack, null);
		sp = 0;
		index = 0;
	}

	public void onFinish() {
		for(int i = 0; i < sp ; i++)
			tree.add(stack[i]);
		attach(tree.toString());
	}
	
	private Expr op1, op2;
	
	@Override
	public void ex_add() {
		op2 = pop();
		op1 = pop();
		push(new Add(op1, op2));
	}
	
	@Override
	public void ex_add_string(int index) {
		op2 = pop();
		op1 = pop();
		push(new Concat(op1, op2));
	}
	
	@Override
	public void ex_and() {
		op2 = pop();
		op1 = pop();
		push(new And(op1, op2));
	}
	
	@Override
	public void ex_assign() {
		op2 = pop();
		op1 = pop();
		push(new Assign(op1, op2));
	}	
	
	@Override
	public void ex_assign4() {
		op2 = pop();
		op1 = pop();
		push(new Assign(op1, op2));
	}
	
	@Override
	public void ex_bit_and() {
		op2 = pop();
		op1 = pop();
		push(new BitAnd(op1, op2));
	}
	
	@Override
	public void ex_bit_or() {
		op2 = pop();
		op1 = pop();
		push(new BitOr(op1, op2));
	}
	
	@Override
	public void ex_branch_false(int label) {
		push(new JFalse(label));
	}
	
	@Override
	public void ex_branch_true(int label) {
		push(new JTrue(label));
	}
	
	@Override
	public void ex_call_super() {
		push(new Super());
	}
	
	@Override
	public void ex_div() {
		op2 = pop();
		op1 = pop();
		push(new Div(op1, op2));
	}
	
	@Override
	public void ex_equal() {
		op2 = pop();
		op1 = pop();
		push(new Equal(op1, op2));
	}
	
	@Override
	public void ex_exit_handler() {
		push(new Return());
	}
	
	@Override
	public void ex_fetch_d() {
		push(new Fetch(pop()));
	}
	
	@Override
	public void ex_fetch_f() {
		push(new Fetch(pop()));
	}
	
	@Override
	public void ex_fetch_i() {
		push(new Fetch(pop()));
	}
	
	@Override
	public void ex_fetch_i4() {
		push(new Fetch(pop()));
	}
	
	@Override
	public void ex_func_call(int index) {
		Function func = 
			scansig 
				? Function.get(index, parseFuncName(getComment()))
				: Function.get(index);
		if(func == null) {
			System.out.println("not found function " +  index  + " " + getComment().trim());
			System.exit(0);
		}
		if(scansig) scan: {
			if(func.complete)
				break scan;
			Instruction i0 = next(1), i1 = next(2), i2 = next(3);
			//template 4 solution 2
			if(i1 == null || i1.is(Instruction.LABEL)) {
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			
			//template 1
			if(i0.is(Instruction.SHIFT_SP) && i1.is(Instruction.SHIFT_SP)) {
				func.argc = Math.abs(i0.operand + i1.operand);
				func.ret = false;
				func.complete = true;
				break scan;
			}
			//template 2
			if(i0.is(Instruction.SHIFT_SP) && i0.operand < -1) {
				func.argc = Math.abs(i0.operand) + 1;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//template 3
			if(i0.not(Instruction.SHIFT_SP)) {
				func.argc = 1;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//template 4
			if(i0.is(Instruction.SHIFT_SP) && i0.operand == -1 && i1.is(Instruction.FUNC_CALL, Instruction.ADD, Instruction.ADD_STRING, Instruction.AND, Instruction.ASSIGN, Instruction.ASSIGN4, Instruction.BIT_AND, Instruction.BIT_OR, Instruction.BRANCH_TRUE, Instruction.BRANCH_FALSE, Instruction.DIV, Instruction.EQUAL, Instruction.GREATER, Instruction.GREATER_EQUAL, Instruction.LESS, Instruction.LESS_EQUAL, Instruction.MOD, Instruction.MUL, Instruction.NEGATE, Instruction.NOT, Instruction.NOT_EQUAL, Instruction.OR, Instruction.SUB)) {
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			
			if(i2 != null && i1.is(Instruction.PUSH_CONST, Instruction.PUSH_CONST_F, Instruction.PUSH_PARAMETER, Instruction.PUSH_STRING) && i2.is(Instruction.FUNC_CALL, Instruction.ADD, Instruction.ADD_STRING, Instruction.AND, Instruction.BIT_AND, Instruction.BIT_OR, Instruction.DIV, Instruction.EQUAL, Instruction.GREATER, Instruction.GREATER_EQUAL, Instruction.LESS, Instruction.LESS_EQUAL, Instruction.MOD, Instruction.MUL, Instruction.NOT_EQUAL, Instruction.OR, Instruction.SUB)) { 
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			//solution 1
			if(!(stack[sp - 1] instanceof Fetch)) {
				func.argc = 2;
				func.ret = true;
				func.complete = true;
				break scan;
			}
			if(sp == 1) {
				//System.out.println(trace() + " SP = 1 " + func.name);
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			Expr expr = stack[sp - 2];
			if(expr instanceof Jump || expr instanceof JFalse || expr instanceof JTrue) {//ternary operator???
				func.argc = 1;
				func.ret = false;
				func.complete = true;
				break scan;
			}
			
			func.argc = 1;
			func.ret = true;
			func.complete = false;
		}	
		final ArrayList<Expr> args = new ArrayList<>();
		for(int i = 0 ; i < func.argc ; i++)
			args.add(pop());
		Collections.reverse(args);
		push(new Call(func, args.toArray(new Expr[args.size()])));
			
	}
	
	@Override
	public void ex_greater() {
		op2 = pop();
		op1 = pop();
		push(new Greater(op1, op2));
	}
	
	@Override
	public void ex_greater_equal() {
		op2 = pop();
		op1 = pop();
		push(new GreaterEqual(op1, op2));
	}
	
	@Override
	public void ex_jump(int label) {
		push(new Jump(label));
	}
	
	@Override
	public void ex_less() {
		op2 = pop();
		op1 = pop();
		push(new Less(op1, op2));
	}
	
	@Override
	public void ex_less_equal() {
		op2 = pop();
		op1 = pop();
		push(new LessEqual(op1, op2));
	}
	
	@Override
	public void ex_mod() {
		op2 = pop();
		op1 = pop();
		push(new Mod(op1, op2));
	}
	
	@Override
	public void ex_mul() {
		op2 = pop();
		op1 = pop();
		push(new Mul(op1, op2));
	}
	
	@Override
	public void ex_negate() {
		push(new Neg(pop()));
	}
	
	@Override
	public void ex_not() {
		push(new Not(pop()));
	}
	
	@Override
	public void ex_not_equal() {
		op2 = pop();
		op1 = pop();
		push(new NotEqual(op1, op2));
	}
	
	@Override
	public void ex_or() {
		op2 = pop();
		op1 = pop();
		push(new Or(op1, op2));
	}
	
	@Override
	public void ex_push_const(int value) {
		push(new IConst(value));
	}
	
	@Override
	public void ex_push_const_f(float value) {
		push(new FConst(value));
	}
	
	@Override
	public void ex_push_event() {
		push(new Event());
	}
	
	@Override
	public void ex_push_parameter(int index) {
		push(new Param(getString(index)));
	}
	
	@Override
	public void ex_push_property(int index) {
		push(new Property(getString(index)));
	}
	
	@Override
	public void ex_push_reg_sp() {
		push(new Reference(stack[sp - 1]));
	}
	
	@Override
	public void ex_push_string(int index) {
		push(new Str(getMapString(index)));
	}
	
	@Override
	public void ex_shift_sp(int value) {
		//sp += value;
	}
	
	@Override
	public void ex_sub() {
		op2 = pop();
		op1 = pop();
		push(new Sub(op1, op2));
	}
	
	@Override
	public void ex_label(int label) {
		labels.add(label);
	}
	
	public Expr proces(Expr expr) {
		labels.forEach(expr::mark);
		labels.clear();
		return expr;
	}
	
	public static final String parseFuncName(String value) {
		value = value.trim();
		return value.substring(5, value.length() - 1);
	}
}
