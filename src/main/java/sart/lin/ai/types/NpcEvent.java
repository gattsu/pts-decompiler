package sart.lin.ai.types;

import java.lang.reflect.Field;

import sart.lin.ai.types.annotation.offset;

public final class NpcEvent {
	@offset(40)
	public SharedCreatureData talker;
	@offset(164)
	public int reply;
	@offset(520)
	public FHTML html0;
	@offset(704)
	public Npc myself;
	
	public static Field find(int offset) {
		for(Field field : NpcEvent.class.getDeclaredFields())
			if(field.isAnnotationPresent(offset.class) && field.getAnnotation(offset.class).value() == offset)
				return field;
		throw new RuntimeException("not found field with offset " + offset);
	}
}
